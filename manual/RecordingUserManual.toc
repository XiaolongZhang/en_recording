\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}% 
\contentsline {chapter}{\numberline {2}Installation and Startup}{3}{chapter.2}% 
\contentsline {section}{\numberline {2.1}installation}{3}{section.2.1}% 
\contentsline {section}{\numberline {2.2}Startup}{7}{section.2.2}% 
\contentsline {chapter}{\numberline {3}Hardware and Environment setup}{11}{chapter.3}% 
\contentsline {section}{\numberline {3.1}Hardware requirement}{11}{section.3.1}% 
\contentsline {section}{\numberline {3.2}Lines connection and environment setup}{12}{section.3.2}% 
\contentsline {subsection}{\numberline {3.2.1}NE speaker position}{13}{subsection.3.2.1}% 
\contentsline {subsection}{\numberline {3.2.2}Standard microphone position}{14}{subsection.3.2.2}% 
\contentsline {subsection}{\numberline {3.2.3}Noise speaker position}{14}{subsection.3.2.3}% 
\contentsline {section}{\numberline {3.3}Driver setting}{15}{section.3.3}% 
\contentsline {chapter}{\numberline {4}Menu}{18}{chapter.4}% 
\contentsline {section}{\numberline {4.1}Database}{18}{section.4.1}% 
\contentsline {section}{\numberline {4.2}License}{19}{section.4.2}% 
\contentsline {section}{\numberline {4.3}Option}{20}{section.4.3}% 
\contentsline {section}{\numberline {4.4}Help}{20}{section.4.4}% 
\contentsline {chapter}{\numberline {5}System Setting}{22}{chapter.5}% 
\contentsline {section}{\numberline {5.1}Recording setting}{22}{section.5.1}% 
\contentsline {section}{\numberline {5.2}Hardware setting}{24}{section.5.2}% 
\contentsline {section}{\numberline {5.3}AVN setting}{25}{section.5.3}% 
\contentsline {section}{\numberline {5.4}Selftest}{26}{section.5.4}% 
\contentsline {section}{\numberline {5.5}Calibration}{27}{section.5.5}% 
\contentsline {chapter}{\numberline {6}Scenario Mangement}{31}{chapter.6}% 
\contentsline {section}{\numberline {6.1}Discrete original vector requirements}{31}{section.6.1}% 
\contentsline {section}{\numberline {6.2}Vector}{33}{section.6.2}% 
\contentsline {section}{\numberline {6.3}Scenario}{35}{section.6.3}% 
\contentsline {section}{\numberline {6.4}Scenario Group}{37}{section.6.4}% 
\contentsline {chapter}{\numberline {7}Recording}{38}{chapter.7}% 
